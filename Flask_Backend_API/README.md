# NarendraBlogsSource

Flask is a lightweight framework that can be practiced to implement web applications and backend API applications as well.
This project is an implementation of REST API to store the student details into database.

**Steps to install this project:**


- To Install this project use "pip Install requirment.txt"
- Create a db folder according to your operating system. and replacce the path in "DATABASE_DIR = "C:\\SqliteDB\\StudentDB"" in db_config.py
- Run db_creation.py and make sure the db file is successfully created at location.
- Now run server_config.py and test API's using postman.

All the information are available on Medium blog.
