import sqlite3 as sq
from db.db_config import *

conn = sq.connect(DATABASE_PATH)
cur = conn.cursor()

conn.execute('CREATE TABLE if not exists ' + TABLE_NAME + ' (' +
             'ID TEXT PRIMARY KEY NOT NULL, '
             'NAME TEXT NOT NULL, '
             'RollNo TEXT NOT NULL, ' 
             'Address TEXT NOT NULL, '
             'CLASS TEXT NOT NULL,'
             'DOB TEXT NOT NULL,'
             'gender TEXT NOT NULL' + ' );')

conn.commit()
conn.close()





